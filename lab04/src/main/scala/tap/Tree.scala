package tap

import scala.annotation.tailrec

trait Tree[+A]

case object EmptyTree extends Tree[Nothing]

final case class Node[A](elem: A, left: Tree[A], right: Tree[A]) extends Tree[A]


object TreeOps:
  @tailrec
  def contains[A <: Comparable[A]](t: Tree[A], v: A): Boolean = t match
    case EmptyTree => false
    case Node(e, l, r) => if (e == v) then true else if e.compareTo(v) < 0 then contains(l, e) else contains(r, v)

  def insert[A <: Comparable[A]](t: Tree[A], v: A): Tree[A] = t match
    case EmptyTree => Node(v, EmptyTree, EmptyTree)
    case Node(elem, left, right) => {
      if elem.compareTo(v) == 0 then t
      else if elem.compareTo(v) < 0 then Node(elem, left, insert(right, v))
      else Node(elem, insert(left, v), right)
    }
