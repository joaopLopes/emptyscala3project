package tap

final case class Student(firstName: String, lastName: String, age: Int):
  def fold(foobar: Boolean)(f: (student: Student) => Boolean): Boolean = ???

object Student:
  private val isValid: (String, String, Int) => Boolean =
    (firstName, lastName, age) => {
      age > 0 && firstName.matches("[a-zA-Z]+") && lastName.matches("[a-zA-Z]+")
    }

  def from(firstName: String, lastName: String, age: Int): Option[Student] = if (isValid(firstName, lastName, age))
    Some(Student(firstName, lastName, age))
  else None

  def from(fullName: String, age: Int): Option[Student] =
    def names = fullName.split(" ")

    if (names.length < 2) None
    else
      Some(Student(names.head, names.last, age))
