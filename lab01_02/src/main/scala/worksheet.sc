def lessThan(first: Int, second: Int): Boolean = {
  first < second
}

lessThan(1, 2)
lessThan(2, 2)
lessThan(3, 2)

// -----------------

def and(first: Boolean, second: Boolean): Boolean = {
  if (first != second)
    false
  else
    first
}

def or(first: Boolean, second: Boolean): Boolean = {
  if (first)
    true
  else
    second
}

assert(and(true, true) == true)
assert(and(true, false) == false)
assert(and(false, true) == false)
assert(and(false, false) == false)
assert(or(false, false) == false)
assert(or(true, true) == true)
assert(or(true, false) == true)
assert(or(false, true) == true)
println("tests passed")

// ---------------------------------

def sumDown(x: Int, sum: Int): Int = {
  if (x == 0)
    sum
  else
    sumDown(x - 1, sum + x)
}
// Test
assert(sumDown(5, 0) == 15)

// ------------------------------

def nSymbol(i: Int, c: Char, s: String): String = {
  if (i == 0)
    s
  else
    nSymbol(i - 1, c, s + c)
}
// Test
assert(nSymbol(5, '*', "") == "*****")

// ------------------------------------

def mult(first: Int, second: Int): Int = {
  if (first == 0 || second == 0)
    0
  else if (first > 0 && second < 0)
    mult(second, first)
  else if (first < 0 && second < 0)
    mult(Math.abs(second), Math.abs(first))
  else if (second == 1)
    first
  else
    first + mult(first, second - 1)
}

assert(mult(4, 3) == 12)
assert(mult(0, 0) == 0)
assert(mult(0, 1) == 0)
assert(mult(1, 0) == 0)
assert(mult(-3, -3) == 9)
assert(mult(-3, 4) == -12)
assert(mult(3, -4) == -12)

// -------------
