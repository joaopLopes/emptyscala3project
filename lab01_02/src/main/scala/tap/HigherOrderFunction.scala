package tap

object HigherOrderFunction:
  def isEven(x: Int): Boolean = x % 2 == 0

  def isOdd(x: Int): Boolean = x % 2 != 0

  def square(x: Int): Int = x * x

  def cube(x: Int): Int = x * x * x

  def isPrime(x: Int): Boolean =
    if (x <= 1)
      false
    else if (x == 2)
      true
    else
      !(2 until x).exists(i => x % i == 0)

  def hof(f: Int => Boolean, m: Int => Int, xs: List[Int]): List[Int] =
    xs.filter(f).map(m)


  def multiply(a: Int)(b: Int): Int = a * b

  def multiply_by_2(a: Int): Int =
    multiply(2)(a)

  def multiply_by_3(a: Int): Int =
    multiply(3)(a)


