package tap

import org.scalatest.funsuite.AnyFunSuite

import scala.language.adhocExtensions

class FunSetSuite extends AnyFunSuite :

  import FunSets.*

  test("contains is implemented") {
    assert(contains(x => true, 100))
  }

  /**
   * When writing tests, one would often like to re-use certain values for multiple
   * tests. For instance, we would like to create an Int-set and have multiple test
   * about it.
   *
   * Instead of copy-pasting the code for creating the set into every test, we can
   * store it in the test class using a val:
   *
   * val s1 = singletonSet(1)
   *
   * However, what happens if the method "singletonSet" has a bug and crashes? Then
   * the test methods are not even executed, because creating an instance of the
   * test class fails!
   *
   * Therefore, we put the shared values into a separate trait (traits are like
   * abstract classes), and create an instance inside each test method.
   *
   */

  trait TestSets:
    val s1 = singletonSet(1)
    val s2 = singletonSet(2)
    val s3 = singletonSet(3)

  /**
   * This test is currently disabled (by using "ignore") because the method
   * "singletonSet" is not yet implemented and the test would fail.
   *
   * Once you finish your implementation of "singletonSet", exchange the
   * function "ignore" by "test".
   */
  test("singletonSet(1) contains 1") {

    /**
     * We create a new instance of the "TestSets" trait, this gives us access
     * to the values "s1" to "s3". 
     */
    new TestSets {
      /**
       * The string argument of "assert" is a message that is printed in case
       * the test fails. This helps identifying which assertion failed.
       */
      assert(contains(s1, 1), "Singleton")
    }
  }

  test("union contains all elements") {
    new TestSets {
      val s = union(s1, s2)
      assert(contains(s, 1), "Union 1")
      assert(contains(s, 2), "Union 2")
      assert(!contains(s, 3), "Union 3")
    }
  }

  test("intersections") {
    new TestSets {
      val sa = intersect(s1, s2)
      val sb = intersect(s1, s1)
      assert(!contains(sa, 1), "Intersect 1")
      assert(!contains(sa, 2), "Intersect 2")
      assert(contains(sb, 1), "Intersect 3")
    }
  }

  test("difference") {
    new TestSets {
      val s = diff(s1, s2)
      assert(contains(s, 1), "Diff 1")
      assert(!contains(s, 2), "Diff 2")
      assert(!contains(s, 3), "Diff 3")
    }
  }

  test("filtering") {
    new TestSets {
      val s = filter(s1, s2)
      assert(!contains(s, 1), "Filter 1")
      assert(!contains(s, 2), "Filter 2")
      assert(!contains(s, 3), "Filter 3")
    }
  }

  test("forall predicate") {
    new TestSets {
      assert(forall(s1, s1))
      assert(!forall(s1, s2))
    }
  }

  test("exists predicate") {
    new TestSets {
      assert(exists(s1, s1))
      assert(!exists(s1, s2))
    }
  }

  test("mapping") {
    new TestSets {
      val s = map(s1, x => x * 2)
      assert(forall(s, s2))
    }
  }

