package tap

import org.scalatest.funsuite.AnyFunSuite
import tap.HigherOrderFunction.*

class HigherOrderFunctionTest extends AnyFunSuite :
  test("testIsPrime") {
    val l = (1 to 30).filter(isPrime(_))
    assert(l === List(2, 3, 5, 7, 11, 13, 17, 19, 23, 29))
  }
  test("testHof") {
    val l = (1 to 5).toList
    assert(hof(isEven, square, l) === List(4, 16))
    assert(hof(isOdd,
      cube, l) === List(1, 27, 125))
    assert(hof(isPrime, square, l) === List(4, 9, 25))
  }
  test("isEven") {
    assert((1 to 15).filter(isEven) === List(2, 4, 6, 8, 10, 12, 14))
  }
  test("isOdd") {
    assert((1 to 15).filter(isOdd) === List(1, 3, 5, 7, 9, 11, 13, 15))
  }
  test("square") {
    assert((1 to 6).map(square) === List(1, 4, 9, 16, 25, 36))
  }

  test("isOdd anonymous") {
    assert((1 to 15).filter(x => x % 2 != 0) === List(1, 3, 5, 7, 9, 11, 13, 15))
  }

  test("square anonymous") {
    assert((1 to 6).map(x => x * x) === List(1, 4, 9, 16, 25, 36))
  }

  test("multiply_by_2") {
    assert((1 to 10).map(multiply_by_2) === List(2, 4, 6, 8, 10, 12, 14, 16, 18, 20))
  }

  test("multiply_by_3") {
    assert((1 to 6).map(multiply_by_3) === List(3, 6, 9, 12, 15, 18))
  }
