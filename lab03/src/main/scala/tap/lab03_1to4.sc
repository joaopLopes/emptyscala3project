import scala.::

//  #1
val aList: List[Int] = List(1, 2, 3)
val bList = List("edom", "odsoft", "tap")
val cList = List('a', 'b')
val dList = List(true, false)
val e = 5.6
val fList = List(1.0, 2, 3)
val g = 'i'

//> a) List[Char] using bList

bList.flatMap(str => str.toCharArray)

//> b)

bList.map(str => str.length)

//> c)

aList ::: bList.flatMap(str => str.toCharArray)
//> d)

dList :+ e

//> e)

fList :+ g

//  #2
//> a)

List.range(1, 11, 2)

//> b)

List.tabulate(5)(n => n + n + 1 )

//  #3

val l = List("Maria", "Ana", "Joana", "Julia", "Paulo", "José")

//> a)

l.filter(name => name.startsWith("Jo"))

//> b)

for (name <- l if name.startsWith("Jo")) yield name

//  #4

val x = (num: Int) => Math.abs(num)
List(1, 2, 3, -1, -2, -3, 0).map(x)
//> Result: List[Int] = List(1, 2, 3, 1, 2, 3, 0)
