package vending

import DomainError.*
import scala.annotation.targetName

// TODO: Create the simple types
//       On name collision (two opaque types in the same file are based on the same type and have equal extension methods),
//       use the  @targetName annotation.
object SimpleTypes:

  type Result[A] = Either[DomainError, A]

  // TODO: Create the opaque type Money, which is an Integer and can never be negative
  // TODO: Extension method to, which returns the corresponding Int
  // TODO: Extension methods for comparison ( <, > ) between Money
  // TODO: Extension methods for math ( +, -, * ) between Money
  // TODO: Extension methods for math ( / ), division by Denomination, returning a Quantity
  // TODO: Extension method isZero, returning a Boolean 
  opaque type Money = Int

  object Money:
    def from(number: Int): Result[Money] =
      if (number < 0) Left(DomainError.InvalidMoney(number))
      else Right(number)

  extension (M: Money)
    @targetName("Money extension")
    def to: Int = M

    def <(money: Money): Boolean =
      M < money

    def >(money: Money): Boolean =
      M > money

    def +(money: Money): Money =
      M + money

    def -(money: Money): Money =
      M - money

    def *(quantity: Quantity): Money =
      M * quantity

    def /(denomination: Denomination): Quantity =
      Quantity.from(M / denomination.value).getOrElse(0)

    @targetName("Money is zero")
    def isZero: Boolean =
      M == 0

  // TODO: Create the opaque type Quantity,  which is an Integer and can never be negative
  // TODO: Extension method to, which returns the corresponding Int
  // TODO: Extension method isZero should also be created for this type
  // TODO: infix Extension method min, which returns the min Quantity
  opaque type Quantity = Int

  object Quantity:
    def from(number: Int): Result[Quantity] =
      if (number < 0) Left(DomainError.InvalidQuantity(number))
      else Right(number)

  extension (Q: Quantity)
    @targetName("Quantity extension")
    def to: Int = Q

    @targetName("Quantity is zero")
    def isZero: Boolean =
      Q == 0

    infix def min(quantity: Quantity): Quantity =
      if (quantity < Q) quantity else Q


  // TODO: Create an enum type for Denomination in which each case has a value in cents
  // TODO: Cases must be created for 1,2,5,10,20,50,100,200 (in cents)
  enum Denomination(val value: Money):
    case oneCent extends Denomination(1)
    case twoCent extends Denomination(2)
    case fiveCent extends Denomination(5)
    case tenCent extends Denomination(10)
    case twentyCent extends Denomination(20)
    case fiftyCent extends Denomination(50)
    case oneEur extends Denomination(100)
    case twoEur extends Denomination(200)
